use std::fmt;
use reqwest::header::InvalidHeaderValue;

#[derive(Debug)] // Allow the use of "{:?}" format specifier
pub enum MyError {
    AuthError(yup_oauth2::error::Error),
    IOError(std::io::Error),
    HttpError(reqwest::Error),
    HttpStatusCodeError(reqwest::StatusCode),
    ParseError,
    // HeaderError,
    // PathError,
}

// Allow this type to be treated like an error
impl std::error::Error for MyError {}

// Allow the use of "{}" format specifier
impl fmt::Display for MyError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            MyError::AuthError(ref e) => write!(f, "Auth Error: {}", e),
            MyError::IOError(ref e) => write!(f, "I/O Error: {}", e),
            MyError::HttpError(ref e) => write!(f, "HTTP Error: {}", e),
            MyError::HttpStatusCodeError(e) => write!(f, "StatusCodeError: {}", e),
            MyError::ParseError => write!(f, "Parse Error"),
            //   MyError::HeaderError => write!(f, "Header Error"),
            //   MyError::PathError => write!(f, "Path Error"),
        }
    }
}

impl From<yup_oauth2::error::Error> for MyError {
    fn from(e: yup_oauth2::error::Error) -> Self {
        MyError::AuthError(e)
    }
}

impl From<std::io::Error> for MyError {
    fn from(e: std::io::Error) -> Self {
        MyError::IOError(e)
    }
}

impl From<reqwest::Error> for MyError {
    fn from(e: reqwest::Error) -> Self {
        MyError::HttpError(e)
    }
}

impl From<serde_json::Error> for MyError {
    fn from(_e: serde_json::Error) -> Self {
        MyError::ParseError
    }
}

impl From<InvalidHeaderValue> for MyError {
    fn from(_: InvalidHeaderValue) -> Self { MyError::ParseError }
}
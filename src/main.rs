use async_recursion::async_recursion;
use dotenv::dotenv;
use serde::Deserialize;
use serde_json::{json, Value};
use std::env;
use std::path::{PathBuf, Path};
// use yup_oauth2::{InstalledFlowAuthenticator, InstalledFlowReturnMethod};

mod error;

use error::MyError as AppError;
use yup_oauth2::AccessToken;

#[tokio::main]
async fn main() -> Result<(), error::MyError> {
    // ENVIRONMENT
    dotenv().ok();
    let path_srv = env::var("path_on_srv").expect("Pls provide path on srv");
    let mut path_local = PathBuf::from(env::var("path_local").expect("Pls provide local path"));
    let token = obtain_access_token().await?;

    let res = walk_folder(&mut path_local, token.as_str(), &path_srv).await;
    match res {
        Ok(_) => {
            println!(" ... thats all, folks!");
            res
        }
        Err(_) => {
            println!("... done - but not well :-( ");
            res
        }
    }
}

async fn obtain_access_token() -> Result<AccessToken, AppError> {
// Service Account Auth
    let service_account_key =
        yup_oauth2::read_service_account_key("service_secret.json".to_string())
            .await?;

    let authenticator = yup_oauth2::ServiceAccountAuthenticator::builder(service_account_key)
        .build()
        .await?;

    let token = authenticator
        .token(&["https://www.googleapis.com/auth/drive"])
        .await?;

    Ok(token)
}

#[async_recursion]
async fn walk_folder(
    path_local: &Path,
    auth_key: &str,
    target_path_id: &str,
) -> Result<(), AppError> {
    // Iterate given path
    for entry in std::fs::read_dir(path_local)? {
        let entry = entry?;

        let filename = entry.file_name();
        let filename = filename.to_str().expect("Filename is not valid as an UTF-8 string");

        let path = entry.path();
        if path.is_dir() {
            println!("folder found for transfer: {}", filename);
            let folder_id = create_folder_if_needed(filename, &auth_key, &target_path_id)
                .await
                .unwrap();
            walk_folder(&path, auth_key.clone(), &folder_id).await?;
        } else {
            println!("file found for transfer: {}", filename);
            transmit_file(&path, auth_key, filename, target_path_id).await?;
            println!("... sent");
            drop_file(&path)?;
            println!(" ... droped successfully");
        }
    }
    Ok(())
}

mod http_requests;

use http_requests::*;

// ---  FOLDER

/**
   Checks if a folder exists. If not, it gets created and the ID of that new folder is returned
*/
async fn create_folder_if_needed(
    sub_folder_name: &str,
    auth_key: &str,
    target_parent_path_id: &str,
) -> Result<String, error::MyError> {
    println!("entering create_if_ needed");


    let url = format!("https://www.googleapis.com/drive/v3/files?q=%27{}%27+in+parents+and+trashed%3Dfalse+and+mimeType%3D%27application%2Fvnd.google-apps.folder%27&fields=files(id%2Cname%2Csize%2CmimeType%2Cparents)", target_parent_path_id);

    let content: FolderContent = get_request(&url, auth_key).await?;

    // We got something like this (raw json):
    // {
    //     "files": [
    //      {
    //       "id": "1SGjvUNBr_Bwhk7_iJRusKw3ECujwO9QD",
    //       "name": "kamera_unterorder_2",
    //       "mimeType": "application/vnd.google-apps.folder",
    //       "parents": [
    //          target_parent_path_id
    //       ]
    //      },
    //      {
    //       "id": "1g1NcQEFtimBPOHgWbQpi3lr8XPh30iei",
    //       "name": "kamera_unterordner",
    //       "mimeType": "application/vnd.google-apps.folder",
    //       "parents": [
    //          target_parent_path_id
    //       ]
    //      }
    //     ]
    //    }
    // that was converted to a Content
    for gfile in content.files {
        // println!("... found name: {}", gfile.name);
        if sub_folder_name == &gfile.name {
            println!("... found id: {} => folder found", &gfile.id);
            let id = gfile.id;
            return Ok(id);
        }
    }

    // if folder does not exist on server, create one and figure out the id
    println!("... nothing found, will start to create a folder");
    let resource = json!({
        "name": sub_folder_name,
        "parents": [target_parent_path_id],
        "mimeType": "application/vnd.google-apps.folder"
    });

    let part = resource.into_part()?;
    let form = reqwest::multipart::Form::new().part("resource", part);


    println!("... sending folder creation request ...");
    let response: GoogleResponse = multipart_post_request(
        "https://www.googleapis.com/upload/drive/v3/files?uploadType=multipart",
        auth_key, form).await?;

    Ok(response.id)

}


use reqwest::multipart::Part as MimePart;


trait ToPart {
    fn into_part(self) -> Result<MimePart, AppError>;
}


impl ToPart for Value {
    fn into_part(self) -> Result<MimePart, AppError> {
        let stringify = self.to_string();
        let part = MimePart::text(stringify).mime_str("application/json")?;
        Ok(part)
    }
}

impl ToPart for Vec<u8> {
    fn into_part(self) -> Result<MimePart, AppError> {
        let part = MimePart::bytes(self);
        Ok(part)
    }
}

// --- FILE
async fn transmit_file(path: &Path, auth_key: &str, file_name: &str, path_id: &str) -> Result<(), error::MyError> {

    let file_bytes = std::fs::read(path.to_str().get_or_insert("Could not parse path"))?;

    let resource = json!({
        "name": file_name,
        "parents": [path_id]
    });

    let resource_name = resource.into_part()?;
    let media_bytes = file_bytes.into_part()?.mime_str("image/jpeg")?;

    let form = reqwest::multipart::Form::new()
        .part("resource", resource_name)
        .part("media", media_bytes);

    let url = "https://www.googleapis.com/upload/drive/v3/files?uploadType=multipart";

    multipart_post_request(url, auth_key, form).await
}

fn drop_file(path: &Path) -> Result<(), error::MyError> {
    Ok(std::fs::remove_file(&path)?)
}

#[derive(Deserialize, Debug)]
struct FolderContent {
    files: Vec<GoogleFile>,
}

#[derive(Deserialize, Debug)]
struct GoogleFile {
    id: String,
    name: String,
    #[serde(rename="mimeType")]
    mime_type: String,
    parents: Option<Vec<String>>,
}

#[derive(Deserialize, Debug)]
struct GoogleResponse {
    kind: String,
    id: String,
    name: String,
    #[serde(rename="mimeType")]
    mime_type: String,
}

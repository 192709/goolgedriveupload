use lazy_static::lazy_static;
use serde::de::DeserializeOwned;
use reqwest::Client ;

use crate::error::MyError as AppError;
use reqwest::header::{ACCEPT, HeaderValue, CONTENT_TYPE};

lazy_static! {
    static ref DEFAULT_CLIENT: Client = Client::new();
    static ref APP_JSON: HeaderValue = HeaderValue::from_str("application/json").unwrap();
}

pub async fn get_request<T: DeserializeOwned>(uri: &str, token: &str) -> Result<T, AppError> {
    let client = &*DEFAULT_CLIENT;
    let content = client
        .get(uri)
        .header(ACCEPT, &*APP_JSON)
        .bearer_auth(token)
        .send()
        .await?
        .json()
        .await?;

    Ok(content)
}

pub async fn multipart_post_request<T : DeserializeOwned>(uri: &str, auth_key: &str, form: reqwest::multipart::Form) -> Result<T, AppError> {

    let cont_type = format!("multipart/related; boundary={}", form.boundary());
    let client = &*DEFAULT_CLIENT;
    let response = client
        .post(uri)
        .bearer_auth(auth_key)
        .header(CONTENT_TYPE, HeaderValue::from_str(&cont_type)?)
        .header(ACCEPT, &*APP_JSON)
        .multipart(form)
        .send()
        .await?;

    if response.status().is_success() {
        // TODO: Special handling for T = () - can be a bit quicker
        let result_object = response.json().await?;
        Ok(result_object)
    } else {
        let status = response.status();
        let body = response.text().await?;
        println!("Server returned http status code {} with response body {}", status, body);
        Err(AppError::HttpStatusCodeError(status))
    }
}